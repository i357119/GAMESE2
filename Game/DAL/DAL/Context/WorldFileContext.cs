﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Models;

namespace DAL
{
    public class WorldFileContext : IWorldContext
    {
        public void AddWorld(World world)
        {

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "text|*.txt";
            saveFileDialog1.Title = "Save to text file";
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName == "")
            {
               throw new LoadFailException("geen bestandsnaam");
            }
            using (StreamWriter writer =
                new StreamWriter(saveFileDialog1.OpenFile()))
            {
                int prevY = 0;
                List<string> line = new List<string>();
                if (world == null)
                {
                    throw new LoadFailException("geen map om op te slaan");
                }
                world.map.cell.Sort(new CellComparer());
                foreach (Cell item in world.map.cell)
                {
                    if (prevY == item.index.Y)
                    {
                        if (item.index.X == world.map.getCellCount().Width - 1)
                        {
                            line.Add(Convert.ToString(item.type));
                        }
                        else
                        {
                            line.Add(Convert.ToString(item.type) + ",");
                        }
                        if (item.index.X == world.map.getCellCount().Width - 1 && item.index.Y == world.map.getCellCount().Height)
                        {
                            foreach (string str in line.ToArray())
                            {
                                writer.Write(str);
                            }
                            prevY = item.index.Y;
                            line.Clear();

                        }
                    }
                    else
                    {
                        line.Add("\r\n" + Convert.ToString(item.type) + ",");
                        foreach (string str in line.ToArray())
                        {
                            writer.Write(str);
                        }

                        prevY = item.index.Y;
                        line.Clear();
                    }
                    
                }

                List<string> iniList = new List<string>();
                Point playerpos = world.player.getPosition();
                List<Enemy> enemyList = world.ReturnEnemies();
                List<Point> enemyPoints = new List<Point>();
                foreach (Enemy item in enemyList)
                {
                    enemyPoints.Add(item.getPosition());
                }
                List<Item> itemList = world.ReturnItems();
                iniList.Add("\r\nPlayer:" + playerpos.X.ToString() + ","+ playerpos.Y.ToString());
                foreach (Point item in enemyPoints)
                {
                    iniList.Add("Enemy:" + item.X.ToString() + "," + item.Y.ToString());
                }
                foreach (Item item in itemList)
                {
                    iniList.Add("Item:" + item.Position.X.ToString() + "," + item.Position.Y.ToString());
                }
                foreach (string item in iniList)
                {
                    writer.WriteLine(item);
                }
            }
        }

        public World GetWorld()
        {
            Player player = new Player(true, new Point(1,1),100);
            Map map = new Map();
            List<Enemy> enemies = new List<Enemy>();
            List<Item> itemList = new List<Item>();

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "text|*.txt";
            openFileDialog1.Title = "Save to text file";
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName == "")
            {
                throw new LoadFailException("Kan file niet laden");

            }
            using (StreamReader reader = new StreamReader(openFileDialog1.FileName))
            {
                string line;
                int xpos = -1;
                int ypos = -1;
                List<Cell> cells = new List<Cell>();
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.StartsWith("Wall") || line.StartsWith("Normal") || line.StartsWith("Goal"))
                    {
                        xpos = 0;

                        ypos++;
                        string[] lines = line.Split(',');
                        foreach (string item in lines)
                        {
                            Cell cel = new Cell(new Point(xpos, ypos), Cell.CreateCellType(item));
                            cells.Add(cel);
                            xpos++;
                        }
                    }
                    else if (line.StartsWith("Player"))
                    {

                        line = line.Replace("Player:", "");
                        string[] lines = line.Split(',');
                        player = new Player(false, new Point(Convert.ToInt32(lines[0]), Convert.ToInt32(lines[1])), 100);
                    }
                    else if (line.StartsWith("Enemy:"))
                    {

                        line = line.Replace("Enemy:", "");
                        string[] lines = line.Split(',');
                        Enemy enemy = new Enemy(new Point(Convert.ToInt32(lines[0]), Convert.ToInt32(lines[1])), 100);
                        enemies.Add(enemy);
                    }
                    else if (line.StartsWith("Item:"))
                    {
                        line = line.Replace("Item:", "");
                        string[] lines = line.Split(',');
                        Armor armor = new Armor(new Point(Convert.ToInt32(lines[0]), Convert.ToInt32(lines[1])));
                        itemList.Add(armor);
                    }
                }
                map = new Map(cells, new Size(xpos, ypos));
            }
            
            World world = new World(map, enemies,player,itemList);
            return world;
        }
    }
}
