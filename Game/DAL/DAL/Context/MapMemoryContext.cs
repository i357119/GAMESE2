﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace DAL
{
    class MapMemoryContext : IMapContext
    {
        private Map map = new Map();
        public void AddMap(Map map)
        {
            this.map = map;
        }

        public Map GetMap()
        {
            return map;
        }
    }
}
