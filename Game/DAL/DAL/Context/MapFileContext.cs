﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using Models; 

namespace DAL
{
    public class MapFileContext : IMapContext
    {

        public void AddMap(Map map)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "text|*.txt";
            saveFileDialog1.Title = "Save to text file";
            saveFileDialog1.ShowDialog();
            using (StreamWriter writer =
                new StreamWriter(saveFileDialog1.OpenFile()))
            {
                int prevY = 0;
                List<string> line = new List<string>();
                map.cell.Sort(new CellComparer());
                foreach (Cell item in map.cell)
                {
                    if (prevY == item.index.Y)
                    {
                        if (item.index.X == map.getCellCount().Width - 1)
                        {
                            line.Add(Convert.ToString(item.type));
                        }
                        else
                        {
                            line.Add(Convert.ToString(item.type) + ",");
                        }
                        if (item.index.X == map.getCellCount().Width - 1 && item.index.Y == map.getCellCount().Height)
                        {
                            foreach (string str in line.ToArray())
                            {
                                writer.Write(str);
                            }
                            prevY = item.index.Y;
                            line.Clear();

                        }
                    }
                    else
                    {
                        line.Add("\r\n" + Convert.ToString(item.type) + ",");
                        foreach (string str in line.ToArray())
                        {
                            writer.Write(str);
                        }

                        prevY = item.index.Y;
                        line.Clear();

                    }
                }
            }

        }
        public Map GetMap()
        {
            Map map = new Map();

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "text|*.txt";
            openFileDialog1.Title = "Save to text file";
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName == "")
            {
                throw new LoadFailException("Kan file niet laden");

            }
            using (StreamReader reader = new StreamReader(openFileDialog1.FileName))
            {
                string line;
                int xpos = -1;
                int ypos = -1;
                List<Cell> cells = new List<Cell>();
                while ((line = reader.ReadLine()) != null)
                {
                    xpos = 0;
                    ypos++;
                    string[] lines = line.Split(',');
                    foreach (string item in lines)
                    {

                        Cell cel = new Cell(new Point(xpos, ypos), Cell.CreateCellType(item));
                        cells.Add(cel);
                        xpos++;
                    }


                }

                map = new Map(cells, new Size(xpos, ypos));
            }
            return map;

        }
    }
}
