﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace DAL
{
    public class WorldRepository
    {
        private IWorldContext context;

        public WorldRepository(IWorldContext context)
        {
            this.context = context;
        }

        public void AddWorld(World world)
        {
            context.AddWorld(world);
        }

        public World GetWorld()
        {
            return context.GetWorld();
        }
    }
}
