﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace DAL
{
     public class MapRepository
    {
        private IMapContext context;

        public MapRepository(IMapContext context)
        {
            this.context = context;
        }
        public void AddMap(Map map)
        {
            context.AddMap(map);
        }
        public Map GetMap()
        {
            return context.GetMap();
        }
    }
}
