﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using Models;

namespace Game_Test
{
    [TestClass]
    public class WorldTest
    {
        [TestMethod]
        public void worldCreate()
        {
            Map map = new Map(new Size(10, 10), new Size(10, 10), new Point(9, 1));
            Enemy enemy = new Enemy(new Point(4, 4), 100);
            Player player = new Player(false, new Point(1, 1), 100); 
            World world = new World(map, 1, player);
        }
        [TestMethod]
        public void worldMethods()
        {
            Map map = new Map(new Size(10, 10), new Size(10, 10), new Point(2, 1));
            Enemy enemy = new Enemy(new Point(4, 4), 100);
            Player player = new Player(false, new Point(2, 3), 100);
            World world = new World(map, 1, player);
            world.update();
            Assert.AreEqual(true, world.gameOver);
            Assert.AreEqual(false, world.gameWon);
            Player playerwin = new Player(false, new Point(2, 1), 100);
            World worldwin = new World(map, 0, playerwin);
            worldwin.update();
            Assert.AreEqual(true, worldwin.gameWon);
        }
    }
}
