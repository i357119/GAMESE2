﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System.Drawing;

namespace Game_Test
{
    [TestClass]
    public class EnemyTest
    {
        [TestMethod]
        public void enemyCreate()
        {
            Enemy enemy = new Enemy(new Point(5,5), 100);
        }
        [TestMethod]
        public void enemyMethods()
        {
            Enemy enemy = new Enemy(new Point(2, 2), 100);
            Player playerupright = new Player(false, new Point(3, 3), 100);
            enemy.attack(playerupright);
            Assert.AreEqual(new Point(3,3), enemy.getPosition());
            Player playerupleft = new Player(false, new Point(2, 2), 100);
            enemy.attack(playerupleft);
            Assert.AreEqual(new Point(2, 2), enemy.getPosition());
            Player playerdownright = new Player(false, new Point(3, 3), 100);
            enemy.attack(playerdownright);
            Assert.AreEqual(new Point(3, 3), enemy.getPosition());
            Player playerdownleft = new Player(false, new Point(2, 4), 100);
            enemy.attack(playerdownleft);
            Assert.AreEqual(new Point(2, 4), enemy.getPosition());         
            enemy.getPosition();
            Player playertest = new Player(false, new Point(5,5), 100 );
            Enemy testenemy = new Enemy(new Point(6,5),100);
            testenemy.attack(playertest);
            enemy.update();
            Player playertest2 = new Player(false, new Point(5,5),100);
            Enemy testenemy2 = new Enemy(new Point(5,4),100 );
            testenemy2.attack(playertest2);
            enemy.setHitPoints(50);
            Assert.AreEqual(50, enemy.getHitPoints());
        }
    }
}
