﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Drawing;
using Models;
namespace Game_Test
{
    [TestClass]
    public class PlayerTest
    {
        [TestMethod]
        public void playerCreation()
        {
            Player player = new Player(false, new Point(1, 1), 100);
        }
        [TestMethod]
        public void playerMethods()
        {
            Player player = new Player(false, new Point(1, 1), 100);
            Map map = new Map(new Size(10, 10), new Size(10, 10), new Point(9, 1));
            Assert.AreEqual(new Point(1, 1), player.getPosition());
            player.interaction(1, map);
            Assert.AreEqual(new Point(1, 1), player.getPosition());
            player.interaction(2, map);
            Assert.AreEqual(new Point(2,1), player.getPosition());
            player.interaction(3, map);
            Assert.AreEqual(new Point(2, 2), player.getPosition());
            player.interaction(4, map);
            Assert.AreEqual(new Point(1, 2), player.getPosition());
            player.interaction(5, map);
            Assert.AreEqual(new Point(1, 2), player.getPosition());
            Assert.AreEqual(100, player.getHitPoints());
            player.setHitPoints(50);
            Assert.AreEqual(50, player.getHitPoints());
        }
    }
}
