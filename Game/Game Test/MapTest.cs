﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

using Models;

namespace Game_Test
{
    [TestClass]
    public class MapTest { 

        [TestMethod]
        public void MapCreation()
        {
            Map map = new Map(new Size(10, 10), new Size(10, 10), new Point(9, 1));
        }
        [TestMethod]
        public void getMapInfo()
        {
            Map map = new Map(new Size(10, 10), new Size(10, 10), new Point(9, 1));
            Assert.AreEqual(new Size(100, 100), map.getMapSize());
            Assert.AreEqual(new Size(10, 10), map.getCellSize());
            Assert.AreEqual(new Size(10, 10), map.getCellCount());
            Assert.AreEqual(new Point(9,1),map.getGoalPosition());
        }
    }
}
