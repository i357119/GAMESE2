﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;
using DAL;

namespace Game
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Player player;
        Graphics g;
        Map map;
        World world;
        Enemy enemy;
        private void btn1_Click(object sender, EventArgs e)
        {
            label6.Visible = false;
            g = panel1.CreateGraphics();
            Size mapSize = new Size(Convert.ToInt32(numericUpDown2.Value), Convert.ToInt32(numericUpDown1.Value));
            Size cellSize = new Size(Convert.ToInt32(numericUpDown4.Value), Convert.ToInt32(numericUpDown3.Value));
            Point goalPosition = new Point(mapSize.Width - 2, 1);
            map = new Map(mapSize, cellSize, goalPosition);
            panel1.Refresh();
            Point enemyPoint = new Point(mapSize.Width -1, mapSize.Height - 1);
            enemy = new Enemy(enemyPoint, 100);
            Point playerPoint = new Point(1, mapSize.Height - 1);
            player = new Player(false, playerPoint, 100);
            int numberofenemies = Convert.ToInt32(numericUpDown5.Value);
            world = new World(map, numberofenemies, player);
            world.draw(g);
            panel1.Width = map.getMapSize().Height * cellSize.Width;
            panel1.Height = map.getMapSize().Height * cellSize.Height;
            panel1.AutoSize = true;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (world.gameOver == false)
            {
                if (keyData == Keys.Up) player.interaction(1, map);
                if (keyData == Keys.Right) player.interaction(2, map);
                if (keyData == Keys.Down) player.interaction(3, map);
                if (keyData == Keys.Left) player.interaction(4, map);
                world.update();
                world.draw(g);
                gamestate();
                label7.Text = "HP: " + world.player.getHitPoints().ToString();
                label8.Text = "Strenght Left: " + world.player.getStrenghtLeft().ToString();
            }
            return base.ProcessCmdKey(ref msg, keyData);
            }
        public void gamestate()
        {

            foreach (Enemy item in world.ReturnEnemies())
            {
                if (player.getPosition() == item.getPosition())
                {
                    Point panelPoint = new Point(panel1.Width / 2, panel1.Location.Y + panel1.Height / 2);
                    Point labelPoint = panel1.PointToClient(panelPoint);
                    label6.Text = "Game Over!";
                    label6.Visible = true;
                    world.setGameOver(true);
                }
            }
            if (player.getPosition() == map.getGoalPosition())
            {
                Point panelPoint = new Point(panel1.Width / 2, panel1.Location.Y + panel1.Height / 2);
                Point labelPoint = panel1.PointToClient(panelPoint);
                label6.Text = "Win!";
                label6.Visible = true;
            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void load_btn_Click(object sender, EventArgs e)
        {
            try
            {
                MapFileContext context = new MapFileContext();
                MapRepository repo = new MapRepository(context);
                map = repo.GetMap();
                label6.Visible = false;
                g = panel1.CreateGraphics();
                Point goalPosition = new Point(map.getMapSize().Width - 1, 1);
                panel1.Refresh();
                Point enemyPoint = new Point(map.getMapSize().Width - 1, map.getMapSize().Height - 1);
                enemy = new Enemy(enemyPoint, 100);
                Point playerPoint = new Point(1, map.getCellCount().Height - 1);
                player = new Player(false, playerPoint, 100);
                int numberofenemies = Convert.ToInt32(numericUpDown5.Value);
                world = new World(map, numberofenemies, player);
                world.draw(g);
                panel1.Width = (map.getMapSize().Height * map.getCellSize().Width);
                panel1.Height = (map.getMapSize().Height * map.getCellSize().Height);
                panel1.AutoSize = true;
            }
            catch (LoadFailException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            MapFileContext context = new MapFileContext();
            MapRepository repo = new MapRepository(context);
            repo.AddMap(map);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (checkBox1.Checked)
            {
               Point MousePosition = e.Location;
                foreach (Cell item in map.cell)
                {
                    int x = map.getCellSize().Width * item.index.X;
                    int y = map.getCellSize().Height * item.index.Y;
                    Point cellPoint = new Point(x,y);
                    Rectangle rec = new Rectangle(cellPoint,map.getCellSize());
                    if (rec.Contains(panel1.PointToClient(Cursor.Position)))

                    {
                        item.setType();
                    }
                }
                map.draw(g);
            }
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            WorldFileContext context = new WorldFileContext();
            WorldRepository repo = new WorldRepository(context);
            try
            {
                repo.AddWorld(world);
            }
            catch(LoadFailException exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            WorldFileContext context = new WorldFileContext();
            WorldRepository repo = new WorldRepository(context);
            world = repo.GetWorld();
            map = world.map;
            player = world.player;
            label6.Visible = false;
            g = panel1.CreateGraphics();
            world.draw(g);
            panel1.Width = (map.getMapSize().Height * map.getCellSize().Width);
            panel1.Height = (map.getMapSize().Height * map.getCellSize().Height);
            panel1.AutoSize = true;
        }
    }
}
  