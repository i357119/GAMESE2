﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Models
{
    public class CellComparer : IComparer<Cell>
    {
        public int Compare(Cell a, Cell b)
        {
            if ((a.index.X == b.index.X) && (a.index.Y == b.index.Y))
            {
                return 0;
            }
            if ((a.index.Y < b.index.Y) || ((a.index.Y == b.index.Y) && (a.index.X < b.index.X)))
            {
                return -1;
            }
            return 1;
        }
    }
}
