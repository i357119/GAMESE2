﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Models
{
    class Health_Pack : Item
    {
        int Health;
        public Health_Pack(Map map) : base(map)
        {
            decimal armorval = Weight / 2;
            this.Health = Convert.ToInt32(Math.Round(armorval, 0));

        }
        public override void Draw(Graphics g, Map map)
        {
            Point temppos = new Point();
            temppos.X = Position.X * map.getCellSize().Width;
            temppos.Y = Position.Y * map.getCellSize().Height;
            SolidBrush Solidbrush = new SolidBrush(Color.Green);
            Point itempoint = temppos;
            Rectangle itemrec = new Rectangle(itempoint, map.getCellSize());
            g.FillRectangle(Solidbrush, itemrec);
            SolidBrush textbrush = new SolidBrush(Color.White);
            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(
               fontFamily,
               9,
               FontStyle.Regular,
               GraphicsUnit.Pixel);
            g.DrawString("Health", font, textbrush, new Point(itempoint.X + 1, itempoint.Y + 5));
        }
        public override void Effect(Player player)
        {
            this.Position = new Point(-1, -1);
            player.setHitPoints(player.getHitPoints() + Health);
        }
    }
}
