﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Models
{
    public abstract class Character
    {
        protected Point position;
        protected int hitPoints;
        public Character(Point position, int hitPoint)
        {
            this.position = position;
            this.hitPoints = hitPoint;
        }
        public abstract Point getPosition();
        public abstract int getHitPoints();
        public abstract void setHitPoints(int hitpoints);


    }
}
