﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Models
{
    public class Enemy : Character
    {
        private int attackRange = 6;

        public Enemy(Point position, int hitPoints) : base(position, hitPoints)
        {
            this.position = position;
            this.hitPoints = hitPoints;
        }

        public void update()
        {

        }
        public void draw(Graphics g, Map map)
        {
            SolidBrush brush = new SolidBrush(Color.Red);
            Point enemypoint = new Point(map.getCellSize().Width * position.X, map.getCellSize().Height * position.Y);
            Rectangle enemyrec = new Rectangle(enemypoint, map.getCellSize());
            g.FillRectangle(brush, enemyrec);

        }
        public void attack(Player player)
        {
            int differenceX = Math.Abs(position.X - player.getPosition().X);
            int differenceY = Math.Abs(position.Y - player.getPosition().Y);
            int difference = Math.Max(differenceX, differenceY);
            if (player.getPosition().X > position.X && difference < attackRange)
            {
                differenceX = player.getPosition().X - position.X;
                position.X += 1;
            }
            else if (player.getPosition().X < position.X && difference < attackRange)
            {
                differenceX = position.X - player.getPosition().X;
                position.X -= 1;
            }
            if (player.getPosition().Y > position.Y && difference < attackRange)
            {
                differenceY = player.getPosition().Y - position.Y;
                position.Y += 1;
            }
            else if (player.getPosition().Y < position.Y && difference < attackRange)
            {
                differenceY = position.Y - player.getPosition().Y;
                position.Y -= 1;
            }
        }
        public override Point getPosition()
        {
            return position;
        }
        public override int getHitPoints()
        {
            return hitPoints;
        }
        public override void setHitPoints(int hitpoints)
        {
            this.hitPoints = hitpoints;
        }

    }
}
