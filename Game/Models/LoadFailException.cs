﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class LoadFailException : Exception
    {
        public LoadFailException()
        {
            
        }
        public LoadFailException(string message): base(message)
        {
            
        }
    }
}
