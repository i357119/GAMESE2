﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using Models;

namespace Models
{
    public class Map
    {
        //fields
        private Size mapSize;
        private Size cellSize;
        Size cellCount;
        private Point goalPosition;
        public List<Cell> cell { get; private set; }
        //constructor
        public Map()
        {

        }
        public Map(Size mapSize, Size cellSize, Point goalposition)
        {
            this.mapSize = mapSize;
            this.mapSize.Width = mapSize.Width * cellSize.Width;
            this.mapSize.Height = mapSize.Height * cellSize.Height;
            this.cellSize = cellSize;
            this.cellCount = mapSize;
            cell = new List<Cell>();
            this.goalPosition = goalposition;

            for (int i = 0; i < cellCount.Width; i++)
            {
                for (int y = 0; y <= cellCount.Height; y++)
                {
                    Cell cel;
                    if (goalposition.X == i && goalposition.Y == y)
                    {
                        Point index = new Point(i, y);
                        cel = new Cell(index, Cell.CellType.Goal);
                    }
                    else if (y == 0 || y == cellCount.Height)
                    {
                        Point index = new Point(i, y);
                        cel = new Cell(index, Cell.CellType.Wall);
                    }
                    else if (i == 0 || i == cellCount.Width - 1)
                    {
                        Point index = new Point(i, y);
                        cel = new Cell(index, Cell.CellType.Wall);
                    }
                    else
                    {
                        Point index = new Point(i, y);
                        cel = new Cell(index, Cell.CellType.Normal);
                    }
                    cell.Add(cel);
                }
            }

        }

        public Map(List<Cell> cell, Size mapsize)
        {
            this.cell = cell;
            this.mapSize = mapsize;
            this.cellSize = new Size(30, 30);
            this.mapSize.Width = mapSize.Width * cellSize.Width;
            this.mapSize.Height = mapSize.Height * cellSize.Height;
            cellCount = mapsize;
            foreach (Cell item in cell)
            {
                if (item.type == Cell.CellType.Goal) this.goalPosition = item.index;
            }

        }
        public void draw(Graphics g)
        {
            Pen mapPen = new Pen(Color.Black);
            Point startLoc = new Point(0, 0);

            Rectangle mapRec = new Rectangle(startLoc, mapSize);
            foreach (Cell item in cell)
            {
                item.draw(g, this);
            }
            g.DrawRectangle(mapPen, mapRec);
        }
        public Size getMapSize()
        {
            return mapSize;
        }
        public Size getCellSize()
        {
            return cellSize;
        }
        public Size getCellCount()
        {
            return cellCount;
        }
        public Point getGoalPosition()
        {
            return goalPosition;
        }

    }
}
