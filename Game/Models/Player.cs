﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace Models
{
    public class Player : Character
    {
        //fields
        private bool powerUp;
        private int strength;
        private List<Item> inventory = new List<Item>();
        public Player(bool powerUp, Point position, int hitPoint): base(position, hitPoint)
        {
            this.strength = 200;
            this.position = position;
            this.powerUp = powerUp;
            this.hitPoints = hitPoint;
        }
        public void draw(Graphics g, Map map)
        {
            SolidBrush brush = new SolidBrush(Color.Blue);
            Point playerpoint = new Point(map.getCellSize().Width * position.X, map.getCellSize().Height * position.Y);
            Rectangle playerec = new Rectangle(playerpoint, map.getCellSize());
            g.FillRectangle(brush, playerec);
        }
        public void interaction(int keyCode, Map map)
        {
            Action action = (Action)keyCode;
            Point temposition = new Point(this.position.X, this.position.Y);
            Point wallPosition = new Point(temposition.X,temposition.Y);
            List<bool> moveList = new List<bool>();
            for (int i = 0; i < 4; i++) moveList.Add(true);
            wallPosition.Y -= 1;
            foreach (Cell item in map.cell) if (item.index == wallPosition && item.type == Cell.CellType.Wall)
                {
                    moveList[0] = false;
                }
            wallPosition.Y += 2;
            foreach (Cell item in map.cell) if (item.index == wallPosition && item.type == Cell.CellType.Wall)
                {
                    moveList[1] = false;
                }
            wallPosition.Y -= 1;
            wallPosition.X += 1;
            foreach (Cell item in map.cell) if (item.index == wallPosition && item.type == Cell.CellType.Wall)
                {
                    moveList[2] = false;
                }
            wallPosition.X -= 2;
            foreach (Cell item in map.cell) if (item.index == wallPosition && item.type == Cell.CellType.Wall)
                {
                    moveList[3] = false;
                }
            switch (action)
            {
                case Action.NoAction:
                    break;
                case Action.MoveUp:
                    if (moveList[0])
                    {
                        temposition.Y -= 1;
                        this.position = temposition;
                    }
                    break;
                case Action.MoveDown:
                    if (moveList[1])
                    {
                        temposition.Y += 1;
                        this.position = temposition;
                    }
                    break;
                case Action.MoveRight:
                    if (moveList[2])
                    {
                        temposition.X += 1;
                        this.position = temposition;
                    }
                    break;

                case Action.MoveLeft:
                    if (moveList[3])
                    {
                        temposition.X -= 1;
                        this.position = temposition;
                    }
                    break;
                case Action.PeformAction:
                    //nog in te vullen
                    break;
                default:
                    break;

            }

        }
        public enum Action
        {
            NoAction,
            MoveUp,
            MoveRight,
            MoveDown,
            MoveLeft,
            PeformAction
        }
        public override Point getPosition()
        {
            return position;
        }
        public override int getHitPoints()
        {
            return hitPoints;
        }
        public override void setHitPoints(int hitpoints)
        {
            this.hitPoints = hitpoints;
        }

        public void pickUp(Item item)
        {
            inventory.Add(item);
        }

        public int getStrenghtLeft()
        {
            int left = strength;
            foreach (Item it in inventory)
            {
                left -= it.Weight;
            }
            return left;
        }

        public int getStrength()
        {
            return strength;
        }
    }
}
