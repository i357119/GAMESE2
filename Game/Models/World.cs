﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Models
{
    public class World
    {
        //fields
        public bool gameWon { get; private set; }
        public bool gameOver { get; private set; }
        public Map map { get; private set; }
        private List<Enemy> enemies = new List<Enemy>();
        private List<Item> items = new List<Item>();
        public Player player { get; private set; }
        //Constructors
        public World(Map map, int numberofenemies, Player player)
        {
            Armor armor = new Armor(map);
            items.Add(armor);
            this.map = map;
            for (int i = 0; i < numberofenemies; i++)
            {
                Enemy enemy = new Enemy(new Point(2 * i + 1, 2), 100);
                enemies.Add(enemy);
            }

            this.player = player;

            Health_Pack healthpack = new Health_Pack(map);
            items.Add(healthpack);
        }

        public World(Map map, List<Enemy> enemies, Player player, List<Item> items  )
        {
            this.map = map;
            this.enemies = enemies;
            this.items = items;
            this.player = player;
        }
        //Methods
        public void update()
        {
            foreach (Enemy item in enemies) item.attack(player);
            if (player.getPosition() == map.getGoalPosition()) gameWon = true;
            foreach (Enemy item in enemies)
            {
                if (player.getPosition() == item.getPosition()) gameOver = true;
            }
            foreach (Item item in items) item.Update(player);
        }
        public void draw(Graphics g)
        {
            map.draw(g);
            player.draw(g, map);
            foreach (Enemy item in enemies) item.draw(g, map);
            foreach (Item item in items) item.Draw(g, map);
        }

        public List<Enemy> ReturnEnemies()
        {
            return enemies;
        }

        public List<Item> ReturnItems()
        {
            return items;
        }

        public void setGameOver(bool bol)
        {
            gameOver = bol;
        }
    }
}
