﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;

namespace Models
{
    public abstract class Item : ICarryable
    {
        public Point Position { get; protected set; }
        public int Weight { get; private set; }

        private static readonly ThreadLocal<Random> appRandom
     = new ThreadLocal<Random>(() => new Random());
        protected Item(Map map)
        {
            Point temppos= new Point(0,0);
            int x = 1;
            for (int i = 0; i < x; i++)
            {
                try
                {
                    temppos = new Point(appRandom.Value.Next(1, map.getCellCount().Width - 1),
                        appRandom.Value.Next(1, map.getCellCount().Height - 1));

                    foreach (Cell item in map.cell)
                    {
                        if (temppos == item.index && item.type == Cell.CellType.Wall)
                        {
                            x++;
                        }
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    
                }
            }
            this.Position = temppos;
            this.Weight = appRandom.Value.Next(100, 120);
        }

        protected Item(Point position)
        {
            this.Position = position;
            this.Weight = appRandom.Value.Next(100, 120);
        }

        public abstract void Draw(Graphics g, Map map);

        public abstract void Effect(Player player);
        public void Update(Player player)
        {
            if(player.getPosition() == Position && player.getStrenghtLeft() > Weight)
            {
                Effect(player);
                player.pickUp(this);
            } 
            
        }

    }
}
