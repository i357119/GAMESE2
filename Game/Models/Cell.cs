﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Models
{
    public class Cell
    {
        //fields
      
        public Point index { get; private set; }
        public CellType type { get; private set; }

        //constructor
        public Cell(Point index, CellType type)
        {
            this.index = index;
            this.type = type;
        }

        public static CellType CreateCellType(string type)
        {
            switch (type)
            {
                case "Normal":
                    return CellType.Normal;
                    
                case "Wall":
                    return CellType.Wall;
                case "Goal":
                    return CellType.Goal;
                default:
                    return CellType.Normal;
                    
            }
        }
        public enum CellType
        {
            Normal,
            Goal,
            Wall,
        }
        public void draw(Graphics g, Map map)
        {
            SolidBrush mypen = new SolidBrush(Color.White);
            Point cellPoint = new Point(index.X * map.getCellSize().Width, index.Y * map.getCellSize().Height);
            Rectangle cellRec = new Rectangle(cellPoint, map.getCellSize());
            if (type == CellType.Normal) mypen.Color = Color.White;
            else if (type == CellType.Wall) mypen.Color = Color.Black;
            else if (type == CellType.Goal) mypen.Color = Color.Gold;
            g.FillRectangle(mypen,cellRec);
        }

        public int CompareTo(Cell other)
        {
            
            return this.CompareTo(other);
        }

        public void setType()
        {
            switch (type)
            {
             case CellType.Normal:
                    this.type = CellType.Wall;
                    break;
                case CellType.Wall:
                    this.type = CellType.Goal;
                    break;
                case CellType.Goal:
                    this.type = CellType.Normal;
                    break;
            }
        }
    }
}
